Pod::Spec.new do |s|
    s.name             = 'YPSDKPOD'
    s.version          = '0.2.0'
    s.summary          = 'A description of YPSDKPOD.'
    s.description      = 'A long description of YPSDKPOD.'

    s.homepage         = 'https://www.intermediait.com'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'Maxi Cassola' => 'mcassola@intermediait.com' }
    s.source           = { :git => 'https://gitlab.com/intermediaIT/ypsdk.git', :tag => s.version.to_s }

    s.ios.deployment_target = '12.0'
    s.swift_version = '5'

    # s.source_files = 'YPSDKPOD/Frameworks/**/*'

    s.ios.vendored_frameworks = 'YappaSDK.xcframework'
    s.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
    s.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }

    s.dependency "Material"
    s.dependency "ObjectMapper"
    s.dependency "AlamofireObjectMapper", '= 6.2.0'
    s.dependency "FlagPhoneNumber"
    s.dependency "AWSLogs"
    s.dependency 'Alamofire', '~> 5.0.0-rc.2'
end
